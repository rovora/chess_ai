# Chess AI with Minimax and Alpha-Beta Pruning

## Overview

The idea behind this project is to experiment with the Minimax algorithm to see if passable chess play can be achieved with
minimal computational overhead.  The average branching factor in chess (number of legal moves in a given position) is somewhere around
30. The average game length is somewhere around 50 moves. Clearly an exhaustive search is not feasible. In this implementation of
Minimax, depth is limited to 3, and a heuristic function is applied to all positions found at this depth to generate position values.
Once the values for the leaf positions are calculated, the Minimax algorithm is applied to generate a "best move".

## Tools and Algorithms

1. The [Python-Chess](https://python-chess.readthedocs.io/en/latest/index.html "Python-Chess" library is used for legal move generation and board display.
2. Wiki for [Minimax] (https://en.wikipedia.org/wiki/Minimax) explanation.
3. Wiki for [Alpha-Beta Pruning] (https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning) explanation. 

