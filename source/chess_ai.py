# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 15:10:03 2018

@author: rohan
"""
#Required Libraries:
#Python-chess
#IPython

#Overview
#This program implements a chess AI. Currently the AI only plays as Black.
#The search algorithm is a Minimax Limited Depth Search at d=3.
#The run command is "vs_ai()"

import chess
import chess.svg
import sys
import search
from IPython.display import SVG, display


#Play a Human Vs. Human match. Both sides input moves. 
def vs_human():
    board = chess.Board()
    print("The game is starting. White to move.")
  
    
    while not board.is_game_over():
        if board.turn == True:
            print("WHITE to move.")
            print(board.legal_moves)
            display(SVG(chess.svg.board(board=board, size=800)))
            choice = get_move(board)
        
            board.push(choice)
            board.turn = False
        else:
            display(SVG(chess.svg.board(board=board)))
            print("BLACK to move.")
            print(board.legal_moves)
            choice = get_move(board)
         
            board.push(choice)
            board.turn = True
            
            
    display(SVG(chess.svg.board(board=board)))
    print("Game Over")

    
    
#Play Vs. AI
def vs_ai():
    board = chess.Board()
    while not board.is_game_over():
        if board.turn == True:
            print("WHITE to move.")
            print(board.legal_moves)
            
            display(SVG(chess.svg.board(board=board, size=400)))

            choice = get_move(board)
            board.push(choice)
            board.turn = False
         
        else:
            move =  search.minimax(board)
            print(move)
            board.push(move)
            board.turn = True
            
    


def get_move(board):
    while True:
        try:
            choice = input("Enter a move:   ")
            if choice == "f":
                print("Player resigns. Game over")
                sys.exit()
            if choice == "u":
                board.pop()
                board.pop()
                
                display(SVG(chess.svg.board(board=board, size=500)))
                candidate = get_move(board)
            else:
                candidate = board.parse_san(choice)
        except ValueError:
            print("Not a valid move! Try again. ")
            continue
        break
    
    
    return candidate

def ai_ai():
    board = chess.Board()
    for i in range(10):
        if board.turn == True:
            
            display(SVG(chess.svg.board(board=board, size=400)))

            move = search.minimax(board)
            board.push(move)
            board.turn = False
         
        else:
            move =  search.minimax(board)
            print(move)
            board.push(move)
            board.turn = True
    
def quick_start():
   print ("Lets Play Chess!")
   print ("")
   print ("")
   print ("Choose one of the following options:")
   print("0 --> Quit")
   print ("1 --> Human 1 Vs Human 2")
   print ("2 --> Human vs AI")
   print("3 --> AI vs AI")
   validation = True
   ##while validation:
   while(validation):
       option = input("Enter a number from the menu: ")
       option = int(option)
       if(option == 1):
           vs_human()
           validation = False
       elif(option == 2):
           vs_ai()
           validation = False
       elif(option == 3):
           ai_ai()
           validation = False
       elif(option == 0):
           validation = False
       else:
           print("Invalid option")
           validation = True



if __name__ == '__main__':
   quick_start()

