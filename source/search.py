# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 16:27:07 2018

@author: rohan
"""

from operator import attrgetter
from random import shuffle
class state:
    def __init__(self, board_state):
        self.position = board_state
        self.value = 0
        self.next = []
        self.beta = 0
        self.alpha = 0
    def choose_min(self):
        lowest = 9999
        shuffle(self.next)
        for i,v in enumerate(self.next):
            if v.value < lowest:
                lowest = v.value
        
        self.value = lowest
        
    def choose_max(self):
        highest = 0
        shuffle(self.next)
        for i,v in enumerate(self.next):
            if v.value > highest:
                highest = v.value
        
        self.value = highest
        
        

def expand(to_expand):
    for i,v in enumerate(to_expand.position.legal_moves):
        node = state(to_expand.position.copy())
        node.position.push(v)
        node.value = pts_heuristic(node.position)
        to_expand.next.append(node)
        
def minimax(board):
    begin_node = state(board)
    expand(begin_node)
    shuffle(begin_node.next)
    for i,v in enumerate(begin_node.next):
        expand(v)
        for i,d in enumerate(v.next):
            expand(d)
            d.choose_max()
            
        v.choose_min()
    candidate = max(begin_node.next, key=attrgetter('value'))
    return candidate.position.pop()
    
        
        


def pts_heuristic(board):
    white_total = 0
    black_total = 0
    
    white_total += len(board.pieces(1, True)) * 1
    white_total += len(board.pieces(2, True)) * 3
    white_total += len(board.pieces(3, True)) * 3
    white_total += len(board.pieces(4, True)) * 5
    white_total += len(board.pieces(5, True)) * 9
    white_total += len(board.pieces(6, True)) * 100
    
    black_total += len(board.pieces(1, False)) * 1
    black_total += len(board.pieces(2, False)) * 3
    black_total += len(board.pieces(3, False)) * 3
    black_total += len(board.pieces(4, False)) * 5
    black_total += len(board.pieces(5, False)) * 9
    black_total += len(board.pieces(6, False)) * 100
    
    if board.turn == False:
        if board.is_checkmate() == True:
            black_total += 1000
        return black_total / white_total
    
    if board.turn == True:
        if board.is_checkmate() == True:
            white_total += 1000
        return white_total / black_total
    
    
