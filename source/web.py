# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 16:20:42 2018

@author: rohan
"""
import os
from flask import Flask, render_template
import chess.svg

app = Flask(__name__)


    
@app.route("/")
def hello_world():
    board = chess.Board()
    svg = chess.svg.board(board=board, size=800)
    return svg
   

if __name__ == "__main__":
    port = int(os.environ.get("PORT",5000))
    app.run(host ='0.0.0.0', port=port)